import { Component } from '@angular/core';

@Component({
  selector: 'app-assignment3',
  templateUrl: './assignment3.component.html',
  styleUrls: ['./assignment3.component.css']
})
export class Assignment3Component {
    showParagraph: boolean = false;
    toggleHistory = []

    constructor() {}

    onToggleParagraph(event: any) {
        this.showParagraph = !this.showParagraph;
        this.toggleHistory.push(
            {
                id: this.toggleHistory.length + 1, 
                timestamp: Date.now(),
                action: (this.showParagraph)? 'show': 'hide'
            }
        )
    }
    
    getColor(toggle) {
        return (toggle.id > 5) ? 'blue' : 'none';
    }
    getCSSClass(toggle) {
        return (toggle.id > 5) ? 'blue' : '';
    }
}
