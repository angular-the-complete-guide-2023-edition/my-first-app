import { Component, Input, ViewChild, ContentChild, ElementRef } from '@angular/core';
import { OnChanges, SimpleChanges } from '@angular/core';
import { OnInit } from '@angular/core';
import { DoCheck } from '@angular/core';
import { AfterContentInit  } from '@angular/core';
import { AfterContentChecked } from '@angular/core';
import { AfterViewInit } from '@angular/core';
import { AfterViewChecked  } from '@angular/core';
import { OnDestroy } from '@angular/core';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html'
})

export class ServerElementComponent implements
  OnInit,
  OnChanges,
  DoCheck,
  AfterContentInit,
  AfterContentChecked,
  AfterViewInit,
  AfterViewChecked,
  OnDestroy {
  @Input('srvElement') element: {type: string, name: string, content: string};
  @Input() name: string;
  @ViewChild('heading', {static: true}) header: ElementRef;
  @ContentChild('contentParagraph', {static: true}) paragraph: ElementRef;

  contructor () {console.log('constructor'); }

  ngOnChanges(changes: SimpleChanges) {
    console.log('ngOnChange', changes);
  }

  ngOnInit() {
    console.log('ngOnInit');
    console.log('Text Content: ' + this.header.nativeElement.textContent);
    console.log('Text Content of paragraph: ' + this.paragraph.nativeElement.textContent);
  }

  ngDoCheck() {
    console.log(' ngDoCheck');
  }

  ngAfterContentInit() {
    console.log('ngAfterContentInit');
    console.log('Text Content of paragraph: ' + this.paragraph.nativeElement.textContent);
  }

  ngAfterContentChecked() {
    console.log(' ngAfterContentChecked');
  }

  ngAfterViewInit() {
    console.log(' ngAfterViewInit');
    console.log('Text Content: ' + this.header.nativeElement.textContent);
  }

  ngAfterViewChecked() { console.log('ngAfterViewChecked'); }

  ngOnDestroy() { console.log(' ngOnDestroy'); }

}
