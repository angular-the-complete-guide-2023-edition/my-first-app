import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
//import { ServerComponent } from './server/server.component';
//import { ServersComponent } from './servers/servers.component';
//import { WarningAlertComponent } from './warning-alert/warning-alert.component';
//import { SuccessAlertComponent } from './success-alert/success-alert.component';
//import { Assignment2Component } from './assignment2/assignment2.component';
//import { Assignment3Component } from './assignment3/assignment3.component';
import { CockpitComponent } from './cockpit/cockpit.component';
import { ServerElementComponent } from './server-element/server-element.component';

@NgModule({
  declarations: [
    AppComponent,
    // ServerComponent,
    // ServersComponent,
    // WarningAlertComponent,
    // SuccessAlertComponent,
    // Assignment2Component,
    // Assignment3Component,
    CockpitComponent,
    ServerElementComponent,
  ],
  imports: [
      BrowserModule
    , FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
